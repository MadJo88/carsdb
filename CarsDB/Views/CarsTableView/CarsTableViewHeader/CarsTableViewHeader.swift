//
//  CarsTableViewHeader.swift
//  CarsDB
//
//  Created by Jo on 6/26/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
    func toggleSection(header: CarsTableViewHeader, section: Int)
}

class CarsTableViewHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var modelLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    @IBOutlet weak var colorLbl: UILabel!
    @IBOutlet weak var showHideLbl: UILabel!
    
    var section = 0
    var isCollapsed = true
    
    weak var delegate: HeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
    }
    @objc private func didTapHeader() {
        delegate?.toggleSection(header: self, section: section)
    }
}
