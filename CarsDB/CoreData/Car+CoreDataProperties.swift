//
//  Car+CoreDataProperties.swift
//  CarsDB
//
//  Created by Jo on 6/28/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//
//

import Foundation
import CoreData


extension Car {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Car> {
        return NSFetchRequest<Car>(entityName: "Car")
    }

    @NSManaged public var color: String
    @NSManaged public var id: Int16
    @NSManaged public var model: String
    @NSManaged public var type: String
    @NSManaged public var isCollapsed: Bool
    @NSManaged public var owners: NSSet?

}

// MARK: Generated accessors for owners
extension Car {

    @objc(addOwnersObject:)
    @NSManaged public func addToOwners(_ value: Owner)

    @objc(removeOwnersObject:)
    @NSManaged public func removeFromOwners(_ value: Owner)

    @objc(addOwners:)
    @NSManaged public func addToOwners(_ values: NSSet)

    @objc(removeOwners:)
    @NSManaged public func removeFromOwners(_ values: NSSet)

}
