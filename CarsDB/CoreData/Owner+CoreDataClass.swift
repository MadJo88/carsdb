//
//  Owner+CoreDataClass.swift
//  CarsDB
//
//  Created by Jo on 6/27/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Owner)
public class Owner: NSManagedObject, Codable {
    enum apiKey: String, CodingKey {
        case id = "owner_id"
        case name = "owner_name"
        case phone = "owner_phone"
    }
    
    // MARK: - Decodable
    required convenience public init(from decoder: Decoder) throws {
        
        ///Fetch context for codable
        guard let codableContext = CodingUserInfoKey.init(rawValue: "context"),
            let manageObjContext = decoder.userInfo[codableContext] as? NSManagedObjectContext,
            let manageObjList = NSEntityDescription.entity(forEntityName: "Owner", in: manageObjContext) else {
                fatalError("failed")
        }
        
        self.init(entity: manageObjList, insertInto: manageObjContext)
        
        let container = try decoder.container(keyedBy: apiKey.self)
        self.id = try container.decodeIfPresent(Int16.self, forKey: .id) ?? 0
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? ""
    }
    
    // MARK: - encode
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: apiKey.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(phone, forKey: .phone)
    }
}
