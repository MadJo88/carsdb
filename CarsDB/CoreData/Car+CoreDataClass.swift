//
//  Car+CoreDataClass.swift
//  CarsDB
//
//  Created by Jo on 6/27/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Car)
public class Car: NSManagedObject, Codable {
    enum apiKey: String, CodingKey {
        case id = "car_id"
        case bodyType = "car_type"
        case model = "car_model"
        case color = "car_color"
        case owners = "owners"
    }
    
    // MARK: - Decodable
    required convenience public init(from decoder: Decoder) throws {
        
        ///Fetch context for codable
        guard let codableContext = CodingUserInfoKey.init(rawValue: "context"),
            let manageObjContext = decoder.userInfo[codableContext] as? NSManagedObjectContext,
            let manageObjList = NSEntityDescription.entity(forEntityName: "Car", in: manageObjContext) else {
                fatalError("failed")
        }
        
        self.init(entity: manageObjList, insertInto: manageObjContext)
        
        let container = try decoder.container(keyedBy: apiKey.self)
        self.id = try container.decodeIfPresent(Int16.self, forKey: .id) ?? 0
        self.type = try container.decodeIfPresent(String.self, forKey: .bodyType) ?? ""
        self.model = try container.decodeIfPresent(String.self, forKey: .model) ?? ""
        self.color = try container.decodeIfPresent(String.self, forKey: .color) ?? ""
        
        self.owners = NSSet(array: try container.decodeIfPresent([Owner].self, forKey: .owners) ?? [Owner]())
    }
    
    // MARK: - encode
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: apiKey.self)
        try container.encode(id, forKey: .id)
        try container.encode(type, forKey: .bodyType)
        try container.encode(model, forKey: .model)
        try container.encode(color, forKey: .color)
    }
}
