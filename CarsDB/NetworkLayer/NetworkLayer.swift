//
//  NetworkLayer.swift
//  CarsDB
//
//  Created by Jo on 6/26/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//
import Foundation

class NetworkLayer{
    static let shared = NetworkLayer()
    
    func getJsonDataByURL() -> Data? {
        guard let fileUrl = Bundle.main.url(forResource: "cars", withExtension: "json") else {
            print("File could not be located at the given url")
            return nil
        }
        
        do {
            let data = try Data(contentsOf: fileUrl)
            return data
        } catch {
            print("Error: \(error)")
            return nil
        }
    }
}
