//
//  CarsViewModel.swift
//  CarsDB
//
//  Created by Jo on 6/26/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//
import Foundation
import CoreData

struct CarsHeaderListViewModel{
    let bodyText: String
    let modelText: String
    let colorText: String
    let owners: [CarsCellListViewModel]
}

struct CarsCellListViewModel {
    let nameText: String
    let phoneText: String
}

class CarsViewModel{
    var carsModel: [Car] = []
    
    private var headerViewModels: [CarsHeaderListViewModel] = [CarsHeaderListViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var numberOfSections: Int {
        return headerViewModels.count
    }
    
    private var cellViewModels: [CarsCellListViewModel] = [CarsCellListViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
    
    func isHeaderCollapsed(at index: Int) -> Bool{
        return carsModel[index].isCollapsed
    }
    
    func getNumberOfCells(at section: Int) -> Int{
        return headerViewModels[section].owners.count
    }
    
    func getHeaderViewModel( at index: Int ) -> CarsHeaderListViewModel {
        return headerViewModels[index]
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> CarsCellListViewModel {
        return headerViewModels[indexPath.section].owners[indexPath.row]
    }
    
    
    private func createCellViewModel(owner: Owner) -> CarsCellListViewModel {
        return CarsCellListViewModel(nameText: owner.name, phoneText: owner.phone)
    }
    
    private func createHeaderViewModel(car: Car ) -> CarsHeaderListViewModel {
        var ownersViewModel = [CarsCellListViewModel]()
        if let owners = car.owners{
            for owner in owners{
                ownersViewModel.append(createCellViewModel(owner: owner as! Owner))
            }
        }
        return CarsHeaderListViewModel(bodyText: car.type,
                                       modelText: car.model,
                                       colorText: car.color,
                                       owners: ownersViewModel)
    }
    
    private func populateHeaderViewModels(cars: [Car] ) {
        var vms = [CarsHeaderListViewModel]()
        for car in cars {
            vms.append(createHeaderViewModel(car: car) )
        }
        self.headerViewModels = vms
    }

    func reverseCollapsedState(at index: Int){
        carsModel[index].isCollapsed = !carsModel[index].isCollapsed
    }

    func setupCodableModel(){
        if let jsonData = NetworkLayer.shared.getJsonDataByURL(){
            clearLocalData()
            parseResponse(forData: jsonData)
        } else {
            fetchLocalData()
        }
        populateHeaderViewModels(cars: carsModel)
    }
    
    ///parse response using decodable and store data
    private func parseResponse(forData jsonData : Data)  {
        do {
            guard let codableContext = CodingUserInfoKey.init(rawValue: "context") else {
                fatalError("Failed context")
            }
            
            let manageObjContext = CoreDataManager.shared.persistentContainer.viewContext
            let decoder = JSONDecoder()
            decoder.userInfo[codableContext] = manageObjContext
            // Parse JSON data
            _ = try decoder.decode([Car].self, from: jsonData)
            ///context save
            try manageObjContext.save()
            UserDefaults.standard.set(true, forKey: "isAllreadyFetch")
            self.fetchLocalData()
        } catch let error {
            print("Error ->\(error.localizedDescription)")
        }
    }
    
    ///Local data fetch from core data
    func fetchLocalData()  {
        let manageObjContext = CoreDataManager.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Car>(entityName: "Car")
        ///Sort by id
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            self.carsModel = try manageObjContext.fetch(fetchRequest)
        } catch let error {
            print(error)
        }
    }
    
    func clearLocalData(){
        let manageObjContext = CoreDataManager.shared.persistentContainer.viewContext
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Car")
        
        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try manageObjContext.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
}
