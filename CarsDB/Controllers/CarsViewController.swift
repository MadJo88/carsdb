//
//  ViewController.swift
//  CarsDB
//
//  Created by Jo on 6/26/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit

class CarsViewController: UIViewController {
    

    @IBOutlet weak var carsTableView: UITableView!
    var isCollapsed = false
    let carsViewModel = CarsViewModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        registerNibs()
        setupCarsViewModel()
    }
    
    @objc private func refreshTableData(){
        setupCarsViewModel()
        carsViewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.carsTableView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.carsTableView.register(UINib(nibName: "CarsTableViewCell", bundle: nil), forCellReuseIdentifier: "CarsTableViewCell")
        self.carsTableView.register(UINib(nibName: "CarsTableViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "CarsTableViewHeader")
    }
    
    private func setupNavigationBar() {
        navigationItem.title = "Cars DB"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(refreshTableData))
    }
    
    private func setupCarsViewModel(){
        carsViewModel.setupCodableModel()
    }
    
    private func setupTableCellView(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = carsTableView.dequeueReusableCell(withIdentifier: "CarsTableViewCell", for: indexPath) as? CarsTableViewCell else { fatalError("Cell view do not exist")}
        let cellViewModel = carsViewModel.getCellViewModel(at: indexPath)
        cell.ownerNameLbl.text = cellViewModel.nameText
        cell.phoneLbl.text = cellViewModel.phoneText
        
        cell.isHidden = carsViewModel.isHeaderCollapsed(at: indexPath.section) ? true : false
        
        return cell
    }
    
    private func setupHeaderView(_ section: Int) -> UIView? {
        guard let header = carsTableView.dequeueReusableHeaderFooterView(withIdentifier: "CarsTableViewHeader") as? CarsTableViewHeader else { fatalError("Header view do not exist")}
        header.section = section
        header.delegate = self
        let headerViewModel = carsViewModel.getHeaderViewModel(at: section)
        header.modelLbl.text = headerViewModel.modelText
        header.bodyLbl.text = headerViewModel.bodyText
        header.colorLbl.text = headerViewModel.colorText
        header.showHideLbl.text = carsViewModel.isHeaderCollapsed(at: section) ? "Show owners" : "Hide owners"
        
        return header
    }
}

extension CarsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return carsViewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carsViewModel.getNumberOfCells(at: section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return setupHeaderView(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return setupTableCellView(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return carsViewModel.isHeaderCollapsed(at: indexPath.section) ? 0.0 : 70.0
    }
}

extension CarsViewController: HeaderViewDelegate{
    func toggleSection(header: CarsTableViewHeader, section: Int) {
        carsViewModel.reverseCollapsedState(at: section)
        carsTableView.beginUpdates()
        carsTableView.reloadSections([section], with: .fade)
        carsTableView.endUpdates()
    }
}

